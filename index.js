'use strict'

const { createPrivateKey, createPublicKey, createSign } = require('crypto')
const { post } = require('axios')

module.exports = class Sendy {
    #privateKey
    #publicKey

    constructor ({ terminalId, privateKey, publicKey }, apiEndpoint = 'https://testapi.sendy.land') {
        this.terminalId = String(terminalId)
        this.apiEndpoint = apiEndpoint

        this.#privateKey = createPrivateKey(privateKey)
        this.#publicKey = createPublicKey(publicKey)
    }

    get publicKey () {
        return this.#publicKey
            .export({
                type: 'spki',
                format: 'der'
            })
            .toString('base64')
    }

    sign (data) {
        return createSign('SHA256')
            .update(data)
            .sign(this.#privateKey, 'base64')
    }

    call (process, data = {}, returnFullResponse = false) {
        if (typeof(data) !== 'object') {
            throw new Error(`Invalid type of payload: ${typeof(data)}\nOnly {Object} allowed`)
        }

        let body = Object.assign(data, {
            Request: process,
            Subject: this.terminalId,
        })

        body = JSON.stringify(body)

        return post(this.apiEndpoint, body,
            {
                headers: {
                    'content-type': 'application/json',
                    'x-signature': this.sign(body),
                },
                validateStatus: function (status) {
                    return status === 200
                }
            })
            .then(response => {
                if (response.data.Errno !== 0) {
                    console.error(response.data)
                    throw new Error(`Failed to API call: ${response.data.Error}`)
                }

                return returnFullResponse
                    ? response
                    : response.data
            })
    }
}
