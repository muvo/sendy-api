[Sendy POS API](https://sandbox.sendy.land/) for NodeJS
=======================================================

# Введение

Модуль упрощает взаимодействие с API [платёжной системы Sendy](https://sendy.land).

# Перед началом

Для работы с API потребуется ID-терминала и ключи шифрования — закрытый и открытый (RSA)

### Создание ключей
Если у вас уже есть пара открытого и закрытого ключа, то этот раздел можно пропустить.

1. Сгенерируйте закрытый ключ:
    ```sh
    openssl genrsa -out private_key.pem 2048
    ```
2. Сгенерируйте открытый ключ на основе закрытого:
    ```sh
    openssl rsa -in private_key.pem -out public_key.pem -pubout
    ```

# Конфигурация
Добавьте модуль в проект:
```sh
npm i @vladismus/sendy
```

В коде вашего проекта:
```js
const Sendy = require('@vladismus/sendy')
const { readFileSync } = require('fs')

// Укажите параметры вашего терминала
const terminal = {
    terminalId: 1234567, // ID терминала посмотрите в Личном Кабинете Sendy
    privateKey: readFileSync('/path/to/your/private_key.pem'),
    publicKey: readFileSync('/path/to/your/public_key.pem'),
}
const apiUrl = undefined // Здесь нужно указать URL к API Sendy, по умолчанию это 'https://testapi.sendy.land'

const api = new Sendy(terminal, apiUrl)
```

# Использование
Обращения к API осуществляются путём вызова функции
```js
api.call(requestName, options, returnFullResponse)
```
Где:
- `requestName` — Идентификатор процесса в Sendy, например `pos/service/activation`
- `options` — Объект **значимых** параметров для конкретного запроса.
    Например, `{Serial: '<S/N of terminal', Secret: '<some string>'}`
    **ОБРАТИТЕ ВНИМАНИЕ**, что параметры API `Subject` (содержит ID терминала) и `Request` (содержит идентификатор процесса) указывать **не нужно** — данные параметры обязательны для всех запросов в Sendy и добавляются в запрос автоматически!
- `returnFullResponse` — булево значение (`true`/`false`); определяет **что** вернуть в ответе — только данные тела ответа от API или **весь** ответ [axios](https://github.com/axios/axios#response-schema) (может потребовать для отладки). По умолчанию это значение равно `false`, то есть, возвращается только тело ответа.

Метод `.call()` является асинхронным и возвращает [JS Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise).

### Примеры использования
Получение данных через `await`:
```js
try {
    const result = await api.call('pos/service/activation', {
        Serial: '1234',
        Secret: '12345678',
        Key: api.publicKey,
    })
    console.log(result)
} catch (error) {
    console.error(error.message)
}
```

Получение данных через `.then()`:
```js
api.call('pos/service/activation', {
        Serial: '1234',
        Secret: '12345678',
        Key: api.publicKey,
    })
    .then(result => {
        console.log(result)
        // some code there
    })
    .catch(error => console.error(error.message))
```

_Показанные выше примеры являются эквивалентными_

# Методы и свойства

- `call(requestName, [options])` —  _Метод_, вполняющий запрос `requestName` в API и возвращающий результат
- `sign(string)` — _Метод_ возвращающий подписанную при помощи закрытого ключа строку `string` (возвращает строку в формате **Base64**)
- `publicKey` — _Свойство_, содержащее открытый ключ в формате **Base64** (Требуется при выполнении запроса на активацию терминала)

# AUTHOR

[Vladislav O. Muschinskikh](https://t.me/VladisMus), email: [<i@vlad.guru>](mailto:i+sendy@vlad.guru)
